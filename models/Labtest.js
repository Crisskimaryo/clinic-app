"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var LabtestSchema = new mongoose_1.Schema({
    timestamp: {
        type: Date,
        default: Date.now
    },
    pid: {
        type: String,
        required: true,
    },
    uid: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Patient',
    },
    group: {
        type: String,
        default: '',
        required: false
    },
    test: {
        type: String,
        default: '',
        required: false
    }, result: {
        type: String,
        default: '',
        required: false
    },
    normalrange: {
        type: String,
        default: '',
        required: false
    },
    unit: {
        type: String,
        default: '',
        required: false
    },
    notes: {
        type: String,
        default: '',
        required: false
    },
    dt: {
        type: Date,
        default: Date.now,
    },
});
exports.default = mongoose_1.model('Labtest', LabtestSchema);
//# sourceMappingURL=Labtest.js.map