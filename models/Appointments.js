"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var AppointmentSchema = new mongoose_1.Schema({
    timestamp: {
        type: Date,
        default: Date.now
    },
    uid: {
        type: mongoose_1.Schema.Types.ObjectId,
        ref: 'Patient',
    },
    status: {
        type: Boolean,
        default: false,
        required: false
    },
    insurancePayment: {
        type: Boolean,
        default: false,
        required: true
    },
    service: {
        type: String,
        default: '',
        required: false
    },
    assigned: {
        type: String,
        default: '',
        required: false
    },
    uuids: {
        type: String,
        unique: true,
        required: true
    },
    date: {
        type: String,
        required: true
    },
});
exports.default = mongoose_1.model('Appointment', AppointmentSchema);
//# sourceMappingURL=Appointments.js.map