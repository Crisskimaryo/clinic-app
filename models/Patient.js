"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var PatientSchema = new mongoose_1.Schema({
    timestamp: {
        type: Date,
        default: Date.now
    },
    pid: {
        type: String,
        required: true,
        unique: true,
    },
    pname: {
        type: String,
        default: '',
        required: true
    },
    sname: {
        type: String,
        default: '',
        required: true
    },
    address: {
        type: String,
        default: '',
        required: false
    },
    gender: {
        type: String,
        default: '',
        required: true
    },
    phone: {
        type: String,
        default: '',
        required: false
    },
    dob: {
        type: Date,
        default: '',
        required: false
    },
    published: {
        type: Boolean,
        default: true,
    },
    dt: {
        type: Date,
        default: Date.now,
    },
    history: [{
            type: mongoose_1.Schema.Types.ObjectId,
            ref: 'history',
            required: false
        }]
});
exports.default = mongoose_1.model('Patient', PatientSchema);
//# sourceMappingURL=Patient.js.map