"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express = require("express");
var mongoose = require("mongoose");
var bodyParser = require("body-parser");
var cookieParser = require("cookie-parser");
var compression = require("compression");
var logger = require("morgan");
var helmet = require("helmet");
var cors = require("cors");
// import our routers/controllers
var PatientRouter_1 = require("./router/PatientRouter");
var HistoryRouter_1 = require("./router/HistoryRouter");
var AppointmentsRouter_1 = require("./router/AppointmentsRouter");
var lab_1 = require("./router/lab");
var Server = /** @class */ (function () {
    function Server() {
        this.app = express();
        this.config();
        this.routes();
    }
    // application config
    Server.prototype.config = function () {
        // const MONGO_URI: string = 'mongodb://localhost:27017/namangadb';
        var MONGO_URI = 'mongodb://heroku_9vnktr0l:4pkb9gt5tjlt170v5kp17hpfle@ds127936.mlab.com:27936/heroku_9vnktr0l';
        mongoose.connect(MONGO_URI || process.env.MONGODB_URI);
        // mongodb://heroku_9vnktr0l:4pkb9gt5tjlt170v5kp17hpfle@ds127936.mlab.com:27936/heroku_9vnktr0l
        // express middleware
        this.app.use(bodyParser.urlencoded({ extended: true }));
        this.app.use(bodyParser.json());
        this.app.use(cookieParser());
        this.app.use(logger('dev'));
        this.app.use(compression());
        this.app.use(helmet());
        this.app.use(cors());
        // cors
        this.app.use(function (req, res, next) {
            res.header('Access-Control-Allow-Origin', '*');
            res.header('Access-Control-Allow-Methods', 'GET, POST, PUT, DELETE, OPTIONS');
            res.header('Access-Control-Allow-Headers', 'Origin, X-Requested-With, Content-Type,application/json, Accept, Authorization, Access-Control-Allow-Credentials');
            res.header('Access-Control-Allow-Credentials', 'true');
            next();
        });
    };
    // application routes
    Server.prototype.routes = function () {
        var router = express.Router();
        this.app.use('/', router);
        this.app.use('/api/v1/patient', PatientRouter_1.default);
        this.app.use('/api/v1/lab', lab_1.default);
        this.app.use('/api/v1/history', HistoryRouter_1.default);
        this.app.use('/api/v1/appointment', AppointmentsRouter_1.default);
    };
    return Server;
}());
// export
exports.default = new Server().app;
//# sourceMappingURL=server.js.map