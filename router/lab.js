"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var Lab_1 = require("../models/Lab");
var LabRouter = /** @class */ (function () {
    function LabRouter() {
        this.router = express_1.Router();
        this.routes();
    }
    // get all of the Labs of patient  in the database
    LabRouter.prototype.thistory = function (req, res) {
        var id = req.params.uid;
        console.log('testhistory');
        console.log(req.params.uid, 'ooooopppp');
        Lab_1.default.find({ uid: { $eq: id } })
            .then(function (data) {
            console.log('hetoooooo');
            // console.log(data)
            res.status(200).json({ data: data });
        })
            .catch(function (error) {
            res.json({ error: error });
        });
    };
    LabRouter.prototype.all = function (req, res) {
        var date = req.params.date;
        date = new Date(date).toDateString();
        // console.log(date)
        console.log('all test');
        Lab_1.default.find({ date: { $eq: date } })
            .populate('uid', 'pid pname sname gender')
            .then(function (data) {
            // console.log(data)
            res.status(200).json({ data: data });
        })
            .catch(function (error) {
            res.json({ error: error });
        });
    };
    // get a single Lab by params of 'slug'
    LabRouter.prototype.one = function (req, res) {
        var id = req.params.uid;
        console.log('one test');
        Lab_1.default.findOne({ _id: id })
            .then(function (data) {
            res.status(200).json({ data: data });
        })
            .catch(function (error) {
            res.status(500).json({ error: error });
        });
    };
    // create a new Lab
    LabRouter.prototype.create = function (req, res) {
        var pid = req.body.pid;
        var uid = req.body.uid;
        var group = req.body.group;
        var test = req.body.test;
        var result = req.body.result;
        var units = req.body.units;
        var nrange = req.body.nrange;
        var notes = req.body.notes;
        var cost = req.body.cost;
        var date = new Date().toDateString();
        var uuids = uid + date;
        console.log(pid);
        if (!uid || !test) {
            res.status(422).json({ message: 'All Fields Required.' });
        }
        var lab = new Lab_1.default({
            pid: pid, uid: uid, date: date, uuids: uuids, group: group, test: test,
            result: result, units: units, nrange: nrange, notes: notes, cost: cost,
        });
        console.log(lab);
        lab.save()
            .then(function (data) {
            res.status(201).json({ data: data });
        })
            .catch(function (error) {
            res.status(500).json({ error: error });
        });
    };
    // update Lab by params of 'slug'
    LabRouter.prototype.update = function (req, res) {
        var id = req.body._id;
        var status = !req.body.status;
        console.log(req.body.status);
        if (req.body.status === undefined) {
            Lab_1.default.findOneAndUpdate({ _id: id }, req.body)
                .then(function (data) {
                res.status(200).json({ data: data });
            })
                .catch(function (error) {
                res.status(500).json({ error: error });
            });
        }
        else {
            Lab_1.default.findOneAndUpdate({ _id: id }, { status: status })
                .then(function (data) {
                res.status(200).json({ data: data });
            })
                .catch(function (error) {
                res.status(500).json({ error: error });
            });
        }
    };
    // delete Lab by params of 'slug'
    LabRouter.prototype.delete = function (req, res) {
        var id = req.params.uid;
        Lab_1.default.findOneAndRemove({ _id: req.params.uid })
            .then(function () {
            res.status(204).end();
        })
            .catch(function (error) {
            res.status(500).json({ error: error });
        });
    };
    LabRouter.prototype.routes = function () {
        this.router.get('/:date', this.all);
        this.router.get('/thistory/:uid', this.thistory);
        this.router.get('/:uid', this.one);
        this.router.post('/', this.create);
        this.router.put('/:uid', this.update);
        this.router.delete('/:uid', this.delete);
    };
    return LabRouter;
}());
exports.LabRouter = LabRouter;
var LabRoutes = new LabRouter();
LabRoutes.routes();
exports.default = LabRoutes.router;
//# sourceMappingURL=lab.js.map