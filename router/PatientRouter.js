"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var Patient_1 = require("../models/Patient");
var PatientRouter = /** @class */ (function () {
    function PatientRouter() {
        this.router = express_1.Router();
        this.routes();
    }
    // get searched value  of the Patients in the database
    PatientRouter.prototype.search = function (req, res) {
        var searched = new RegExp(req.body.svalue, 'i');
        Patient_1.default.find().or([{ 'pid': { $regex: searched } }, { 'pname': { $regex: searched } }, { 'phone': { $regex: searched } }, { 'sname': { $regex: searched } }]).limit(50)
            .then(function (data) {
            res.status(200).json({ data: data });
        })
            .catch(function (error) {
            res.json({ error: error });
        });
    };
    // get all of the Patients in the database
    PatientRouter.prototype.all = function (req, res) {
        Patient_1.default.find().sort({ 'pid': -1 }).limit(100)
            .then(function (data) {
            res.status(200).json({ data: data });
        })
            .catch(function (error) {
            res.json({ error: error });
        });
    };
    // get latest id of the Patients in the database
    PatientRouter.prototype.generateId = function (req, res) {
        Patient_1.default.find().sort({ 'pid': -1 }).limit(1)
            .then(function (data) {
            console.log(data['pid']);
            res.status(200).json({ data: data });
        })
            .catch(function (error) {
            res.json({ error: error });
        });
    };
    // get a single Patient by params of 'slug'
    PatientRouter.prototype.one = function (req, res) {
        var slug = req.params.slug;
        Patient_1.default.findOne({ slug: slug })
            .then(function (data) {
            res.status(200).json({ data: data });
        })
            .catch(function (error) {
            res.status(500).json({ error: error });
        });
    };
    // create a new Patient
    PatientRouter.prototype.create = function (req, res) {
        var pid = req.body.pid;
        var pname = req.body.pname;
        var sname = req.body.sname;
        var address = req.body.address;
        var phone = req.body.phone;
        var gender = req.body.gender;
        var dob = req.body.dob;
        var dt = req.body.dt;
        // const slug: boolean = req.body.slug;
        if (!pid || !pname) {
            res.status(422).json({ message: 'All Fields Required.' });
        }
        var patient = new Patient_1.default({
            pid: pid,
            pname: pname,
            sname: sname,
            address: address,
            phone: phone,
            gender: gender,
            dob: dob,
            dt: dt,
        });
        patient.save()
            .then(function (data) {
            res.status(201).json({ data: data });
        })
            .catch(function (error) {
            res.status(500).json({ error: error });
        });
    };
    // update Patient by params of 'slug'
    PatientRouter.prototype.update = function (req, res) {
        try {
            var id = req.body.uid;
            console.log(req.body.uid);
            Patient_1.default.findOneAndUpdate({ _id: id }, req.body)
                .then(function (data) {
                res.status(200).json({ data: data });
            })
                .catch(function (error) {
                res.status(500).json({ error: error });
            });
        }
        catch (error) {
            console.log(error);
        }
    };
    // delete Patient by params of 'slug'
    PatientRouter.prototype.delete = function (req, res) {
        var id = req.params.uid;
        try {
            Patient_1.default.findOneAndRemove({ _id: req.params.uid })
                .then(function () {
                res.status(204).end();
            })
                .catch(function (error) {
                res.status(500).json({ error: error });
            });
        }
        catch (error) {
            console.log(error);
        }
    };
    PatientRouter.prototype.routes = function () {
        this.router.get('/', this.all);
        this.router.post('/search', this.search);
        this.router.get('/getid', this.generateId);
        this.router.get('/:slug', this.one);
        this.router.post('/', this.create);
        this.router.put('/:slug', this.update);
        this.router.delete('/:uid', this.delete);
    };
    return PatientRouter;
}());
exports.PatientRouter = PatientRouter;
var PatientRoutes = new PatientRouter();
PatientRoutes.routes();
exports.default = PatientRoutes.router;
//# sourceMappingURL=PatientRouter.js.map