"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var History_1 = require("../models/History");
var HistoryRouter = /** @class */ (function () {
    function HistoryRouter() {
        this.router = express_1.Router();
        this.routes();
    }
    // get all of the Historys of patient  in the database
    HistoryRouter.prototype.all = function (req, res) {
        var id = req.params.uid;
        History_1.default.find({ uid: id })
            .then(function (data) {
            res.status(200).json({ data: data });
        })
            .catch(function (error) {
            res.json({ error: error });
        });
    };
    // get a single History by params of 'slug'
    HistoryRouter.prototype.one = function (req, res) {
        var id = req.params.uid;
        History_1.default.findOne({ _id: id })
            .then(function (data) {
            res.status(200).json({ data: data });
        })
            .catch(function (error) {
            res.status(500).json({ error: error });
        });
    };
    // create a new History
    HistoryRouter.prototype.create = function (req, res) {
        var pid = req.body.pid;
        var uid = req.body.uid;
        var complain = req.body.complain;
        var mph = req.body.mph;
        var invst = req.body.invst;
        var drug = req.body.drug;
        var exam = req.body.exam;
        var dt = req.body.dt;
        // const slug: boolean = req.body.slug;
        if (!pid || !complain) {
            res.status(422).json({ message: 'All Fields Required.' });
        }
        var history = new History_1.default({
            pid: pid,
            uid: uid,
            complain: complain,
            mph: mph,
            invst: invst,
            drug: drug,
            exam: exam,
            dt: dt,
        });
        history.save()
            .then(function (data) {
            res.status(201).json({ data: data });
        })
            .catch(function (error) {
            res.status(500).json({ error: error });
        });
    };
    // update History by params of 'slug'
    HistoryRouter.prototype.update = function (req, res) {
        var id = req.params.uid;
        History_1.default.findOneAndUpdate({ _id: id }, req.body)
            .then(function (data) {
            res.status(200).json({ data: data });
        })
            .catch(function (error) {
            res.status(500).json({ error: error });
        });
    };
    // delete History by params of 'slug'
    HistoryRouter.prototype.delete = function (req, res) {
        var id = req.params.uid;
        History_1.default.findOneAndRemove({ _id: req.params.uid })
            .then(function () {
            res.status(204).end();
        })
            .catch(function (error) {
            res.status(500).json({ error: error });
        });
    };
    HistoryRouter.prototype.routes = function () {
        this.router.get('/:uid', this.all);
        this.router.get('/:uid', this.one);
        this.router.post('/', this.create);
        this.router.put('/:uid', this.update);
        this.router.delete('/:uid', this.delete);
    };
    return HistoryRouter;
}());
exports.HistoryRouter = HistoryRouter;
var HistoryRoutes = new HistoryRouter();
HistoryRoutes.routes();
exports.default = HistoryRoutes.router;
//# sourceMappingURL=HistoryRouter.js.map