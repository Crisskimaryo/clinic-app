"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
var HistorySchema = new mongoose_1.Schema({
    timestamp: {
        type: Date,
        default: Date.now
    },
    pid: {
        type: String,
        required: true,
    },
    complain: {
        type: String,
        default: '',
        required: true
    },
    mph: {
        type: String,
        default: '',
        required: false
    },
    invst: {
        type: String,
        default: '',
        required: false
    },
    drug: {
        type: String,
        default: '',
        required: false
    },
    exam: {
        type: String,
        default: '',
        required: false
    },
    dt: {
        type: Date,
        default: Date.now,
    },
});
exports.default = mongoose_1.model('History', HistorySchema);
//# sourceMappingURL=History.js.map