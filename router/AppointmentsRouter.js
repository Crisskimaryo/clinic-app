"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var express_1 = require("express");
var Appointments_1 = require("../models/Appointments");
var AppointmentRouter = /** @class */ (function () {
    function AppointmentRouter() {
        this.router = express_1.Router();
        this.routes();
    }
    // get all of the Appointments of patient  in the database
    AppointmentRouter.prototype.all = function (req, res) {
        var date = req.params.date;
        date = new Date(date).toDateString();
        console.log(date);
        Appointments_1.default.find({ date: { $eq: date } }).populate('uid', 'pid pname sname gender')
            .then(function (data) {
            res.status(200).json({ data: data });
        })
            .catch(function (error) {
            res.json({ error: error });
        });
    };
    // get a single Appointment by params of 'slug'
    AppointmentRouter.prototype.one = function (req, res) {
        var id = req.params.uid;
        Appointments_1.default.findOne({ _id: id })
            .then(function (data) {
            res.status(200).json({ data: data });
        })
            .catch(function (error) {
            res.status(500).json({ error: error });
        });
    };
    // create a new Appointment
    AppointmentRouter.prototype.create = function (req, res) {
        var uid = req.body.uid;
        var insurancePayment = req.body.insurancePayment;
        var date = req.body.date;
        var uuids = uid + date;
        var assigned = req.body.assigned;
        var service = req.body.service;
        if (!uid || !date && !uuids) {
            res.status(422).json({ message: 'All Fields Required.' });
        }
        var appointment = new Appointments_1.default({
            uid: uid,
            date: date,
            uuids: uuids,
            insurancePayment: insurancePayment,
            assigned: assigned,
            service: service
        });
        appointment.save()
            .then(function (data) {
            res.status(201).json({ data: data });
        })
            .catch(function (error) {
            res.status(500).json({ error: error });
        });
    };
    // update Appointment by params of 'slug'
    AppointmentRouter.prototype.update = function (req, res) {
        var id = req.body._id;
        var status = !req.body.status;
        if (status != undefined) {
            Appointments_1.default.findOneAndUpdate({ _id: id }, { status: status })
                .then(function (data) {
                res.status(200).json({ data: data });
            })
                .catch(function (error) {
                res.status(500).json({ error: error });
            });
        }
    };
    // delete Appointment by params of 'slug'
    AppointmentRouter.prototype.delete = function (req, res) {
        var id = req.params.uid;
        Appointments_1.default.findOneAndRemove({ _id: req.params.uid })
            .then(function () {
            res.status(204).end();
        })
            .catch(function (error) {
            res.status(500).json({ error: error });
        });
    };
    AppointmentRouter.prototype.routes = function () {
        this.router.get('/:date', this.all);
        this.router.get('/:uid', this.one);
        this.router.post('/', this.create);
        this.router.put('/:uid', this.update);
        this.router.delete('/:uid', this.delete);
    };
    return AppointmentRouter;
}());
exports.AppointmentRouter = AppointmentRouter;
var AppointmentRoutes = new AppointmentRouter();
AppointmentRoutes.routes();
exports.default = AppointmentRoutes.router;
//# sourceMappingURL=AppointmentsRouter.js.map